# plot_ENV.jl

NeuroAnalyzer plugin: plot envelope.

This software is licensed under [The 2-Clause BSD License](LICENSE).

## Usage

Amplitude:
```julia
p1 = plot(eeg, ch="Fp1")
p2 = plot_env(eeg, ch="Fp1", ep=1, type=:amp)
p = Plots.plot(p1, p2, layout=(2, 1))
plot_save(p, file_name="amp.png")
```

![](amp.png)

Power spectrum:
```julia
p1 = plot_psd(eeg, ch="Fp1", ep=1)
p2 = plot_env(eeg, ch="Fp1", ep=1, type=:pow)
p = Plots.plot(p1, p2, layout=(2, 1))
plot_save(p, file_name="psd.png")
```

![](psd.png)

Spectrogram:
```julia
p1 = plot_spectrogram(eeg, ch="Fp1", ep=1, frq_lim=(0, 30), cb=false)
p2 = plot_env(eeg, ch="Fp1", ep=1, type=:spec, frq_lim=(0, 30))
p = Plots.plot(p1, p2, layout=(2, 1))
plot_save(p, file_name="spec.png")
```

![](spec.png)